package com.pruebatecnica.callcenter.callcenter;

import java.util.Calendar;
import java.util.Map;

import com.pruebatecnica.callcenter.callcenter.model.Agent;
import com.pruebatecnica.callcenter.callcenter.model.Call;
import com.pruebatecnica.callcenter.callcenter.model.CallService;
import com.pruebatecnica.callcenter.callcenter.model.ICallService;
import com.pruebatecnica.callcenter.callcenter.model.Role;
import com.pruebatecnica.callcenter.callcenter.model.Role.AgentServiceLeveL;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Valida que los operadores hayan recibido las primeras 8 llamadas y las siguientes se escalaran a los siguientes roles.
     * Se lanzan 10 llamadas
     */
    public void testAttentionPriority()
    {
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "1"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "2"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "3"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "4"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "5"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "6"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "7"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "8"));
        CallService.getInstance().addCallAgent(new Agent(new Role("SUPERVISOR",Role.AgentServiceLeveL.SUPERVISOR), "9"));
        CallService.getInstance().addCallAgent(new Agent(new Role("SUPERVISOR",Role.AgentServiceLeveL.DIRECTOR), "10"));
        for(int i=0;i<10;i++){
        	Call call= new Call();
        	call.setId(i+"");
        	call.setStartTime(Calendar.getInstance().getTime().getTime());
        	ICallService service=CallService.getInstance();
        	Thread launcherThread= new Thread(new CallLauncher(service, call));
        	launcherThread.start();
        }
        try {
			Thread.sleep(10*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Map<String,Integer> stats=CallService.getInstance().getStats();
        int operador=stats.get(AgentServiceLeveL.OPERADOR.name());
        int supervisor=stats.get(AgentServiceLeveL.SUPERVISOR.name());
        int director=stats.get(AgentServiceLeveL.DIRECTOR.name());
        int encolados=stats.containsKey(AgentServiceLeveL.COLA.name())?stats.get(AgentServiceLeveL.COLA.name()):0;

        //8 atendidos por operadores, 1 por el Supervisor y 1 por el director, ninguno encolado
        assertTrue( operador==8&& supervisor==1&&director==1&&encolados==0);
    }

    public void testCallQueue()
    {
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "1"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "2"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "3"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "4"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "5"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "6"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "7"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "8"));
        CallService.getInstance().addCallAgent(new Agent(new Role("SUPERVISOR",Role.AgentServiceLeveL.SUPERVISOR), "9"));
        CallService.getInstance().addCallAgent(new Agent(new Role("SUPERVISOR",Role.AgentServiceLeveL.DIRECTOR), "10"));
        for(int i=0;i<20;i++){
        	Call call= new Call();
        	call.setId(i+"");
        	call.setStartTime(Calendar.getInstance().getTime().getTime());
        	ICallService service=CallService.getInstance();
        	Thread launcherThread= new Thread(new CallLauncher(service, call));
        	launcherThread.start();
        }
        try {
        	//Si corre antes de terminar la atencion no da resultados precisos
			Thread.sleep(40*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Map<String,Integer> stats=CallService.getInstance().getStats();
        int operador=stats.get(AgentServiceLeveL.OPERADOR.name());
        int supervisor=stats.get(AgentServiceLeveL.SUPERVISOR.name());
        int director=stats.get(AgentServiceLeveL.DIRECTOR.name());
        int encolados=stats.containsKey(AgentServiceLeveL.COLA.name())?stats.get(AgentServiceLeveL.COLA.name()):0;

        //8 atendidos por operadores, 1 por el Supervisor y 1 por el director, ninguno encolado
        assertTrue("encolados="+encolados+"   operador="+operador+" supervisor="+supervisor+" director="+director,operador>9);
    }
    public void testVoiceMailTest()
    {
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "1"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "2"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "3"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "4"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "5"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "6"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "7"));
        CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR",Role.AgentServiceLeveL.OPERADOR), "8"));
        CallService.getInstance().addCallAgent(new Agent(new Role("SUPERVISOR",Role.AgentServiceLeveL.SUPERVISOR), "9"));
        CallService.getInstance().addCallAgent(new Agent(new Role("SUPERVISOR",Role.AgentServiceLeveL.DIRECTOR), "10"));
        for(int i=0;i<25;i++){
        	Call call= new Call();
        	call.setId(i+"");
        	call.setStartTime(Calendar.getInstance().getTime().getTime());
        	ICallService service=CallService.getInstance();
        	Thread launcherThread= new Thread(new CallLauncher(service, call));
        	launcherThread.start();
        }
        try {
        	//Si corre antes de terminar la atencion no da resultados precisos
			Thread.sleep(45*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Map<String,Integer> stats=CallService.getInstance().getStats();
        int operador=stats.get(AgentServiceLeveL.OPERADOR.name());
        int supervisor=stats.get(AgentServiceLeveL.SUPERVISOR.name());
        int director=stats.get(AgentServiceLeveL.DIRECTOR.name());
        int encolados=stats.containsKey(AgentServiceLeveL.COLA.name())?stats.get(AgentServiceLeveL.COLA.name()):0;

        //5 llamadas seran desconectadas pq la cola esta llena y no hay operadores disponibles
        assertTrue("encolados="+encolados+"   operador="+operador+" supervisor="+supervisor+" director="+director,true);
    }
}

package com.pruebatecnica.callcenter.callcenter.model;

import java.util.Calendar;

import com.pruebatecnica.callcenter.callcenter.model.Call.CallStatus;

public class VoiceMail extends Agent {

/**
	 * 
	 */
	private static final long serialVersionUID = -4317503702864622L;

public VoiceMail(String id){
	super(null, id);
}
@Override
	public Call connectCall(Call incomingCall) {
		System.out.println("---- Se recibio un Mensaje de Voz para la llamada con id "+incomingCall.getId());
		incomingCall.setStatus(CallStatus.DISCONNECTED);
		incomingCall.setEndTime(Calendar.getInstance().getTime().getTime());
		return incomingCall;
	}

}

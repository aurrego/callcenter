package com.pruebatecnica.callcenter.callcenter.rule;

import com.pruebatecnica.callcenter.callcenter.model.Call;
import com.pruebatecnica.callcenter.callcenter.model.Dispatcher;
import com.pruebatecnica.callcenter.callcenter.model.ICallAgent;
import com.pruebatecnica.callcenter.callcenter.model.ICallAgent.AgentStatus;
import com.pruebatecnica.callcenter.callcenter.model.Role.AgentServiceLeveL;
/**
 * Resuelve la Segunda instancia de atención de llamadas (Supervisor)
 * */
public class CallAgentsLevel2 implements IProcessCall {

	public boolean process(Dispatcher dispatcher, Call incomingCall) {
		ICallAgent agent=dispatcher.getAgentQueueSerLevel2().poll();
		if(agent!=null){
			//TODO Poner un logger
			System.out.println("LLamada "+incomingCall.getId()+" asignada a un "+agent.getRole().getServiceLevel()+" con id "+agent.getId()+". Quedan "+dispatcher.getAgentQueueSerLevel2().size()+" "+agent.getRole().getServiceLevel()+" disponibles");
			agent.setStatus(AgentStatus.BUSY);
			 agent.connectCall(incomingCall);
			 dispatcher.addStats(AgentServiceLeveL.SUPERVISOR);
			 return true;
		}
		CallAgentsLevel3 ruleProcess= new CallAgentsLevel3();
		return ruleProcess.process(dispatcher, incomingCall);
	}

}

package com.pruebatecnica.callcenter.callcenter.model;

import java.util.Map;
/**
 * Fachada del servicio de atención de llamadas
 * */
public interface ICallService {
	boolean receiveCall(Call incomingCall);
	boolean addCallAgent(ICallAgent agent);
	Map<String,Integer> getStats();


}

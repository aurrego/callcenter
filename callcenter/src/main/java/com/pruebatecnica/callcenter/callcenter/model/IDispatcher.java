package com.pruebatecnica.callcenter.callcenter.model;

public interface IDispatcher {

	boolean dispatchCall(Call call);
}

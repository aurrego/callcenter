package com.pruebatecnica.callcenter.callcenter.model;

import java.util.Map;

public class CallService implements ICallService {
	private static CallService serviceInstance;
	private CallService(){}
	
	private Dispatcher dispacher= new Dispatcher();
	public boolean receiveCall(Call incomingCall) {
		System.out.println("Lamada con Id "+incomingCall.getId()+" fue recibida.");
		return dispacher.dispatchCall(incomingCall);
	}
	@Override
	public boolean addCallAgent(ICallAgent agent){
		return dispacher.addAgent(agent);
		
	}
	public static CallService  getInstance(){
		if(serviceInstance==null){
			serviceInstance= new CallService();
		}
		return serviceInstance;
	}


	@Override
	public Map<String, Integer> getStats() {
		
		return dispacher.getAttentionStats();
	}
}

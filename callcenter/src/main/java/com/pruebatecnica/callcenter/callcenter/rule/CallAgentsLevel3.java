package com.pruebatecnica.callcenter.callcenter.rule;

import com.pruebatecnica.callcenter.callcenter.model.Call;
import com.pruebatecnica.callcenter.callcenter.model.Dispatcher;
import com.pruebatecnica.callcenter.callcenter.model.ICallAgent;
import com.pruebatecnica.callcenter.callcenter.model.ICallAgent.AgentStatus;
import com.pruebatecnica.callcenter.callcenter.model.Role.AgentServiceLeveL;
/**
 * Resuelve la tercera instancia de atención de llamadas (Director)
 * */
public class CallAgentsLevel3 implements IProcessCall {

	public boolean process(Dispatcher dispatcher, Call incomingCall) {
		ICallAgent agent=dispatcher.getAgentQueueSerLevel3().poll();
		if(agent!=null){
			//TODO Poner un logger
			System.out.println("LLamada "+incomingCall.getId()+" asignada a un "+agent.getRole().getServiceLevel()+" con id "+agent.getId()+". Quedan "+dispatcher.getAgentQueueSerLevel3().size()+" "+agent.getRole().getServiceLevel()+" disponibles");
			agent.setStatus(AgentStatus.BUSY);
			 agent.connectCall(incomingCall);
			 dispatcher.addStats(AgentServiceLeveL.DIRECTOR);
			 return true;
		}
		CallQueueRule ruleProcess= new CallQueueRule();
		return ruleProcess.process(dispatcher, incomingCall);
	}

}

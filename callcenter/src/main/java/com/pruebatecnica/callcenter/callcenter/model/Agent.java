package com.pruebatecnica.callcenter.callcenter.model;

import java.io.Serializable;
import java.util.ArrayList;

import com.pruebatecnica.callcenter.callcenter.CallProcesor;

public class Agent implements Serializable,ICallAgent {

	private ArrayList<ICallListener> listenersList= new ArrayList<ICallListener>();
	private AgentStatus status=AgentStatus.AVAILABLE;
	final Role role;
	final String  id;
	private static final long serialVersionUID = -528657094225531131L;

	public Agent(Role role,String id) {
		this.role=role;
		this.id=id;
	}
	public Call connectCall(Call incomingCall) {
		incomingCall.setStatus(Call.CallStatus.CONNECTED);
		// TODO Auto-generated method stub
		Thread callThread= new Thread(new CallProcesor(this,incomingCall));
		callThread. start();
		return incomingCall;
	}

	public boolean addStatusListener(ICallListener listener) {
		if(listener!=null){
			listenersList.add(listener);
		}
		return false;
	}

	public boolean removeStatusListener(ICallListener listener) {
		if(listener!=null){
			return listenersList.remove(listener);
		}
		return false;
	}

	public void notifyStatusListeners() {

		for(ICallListener listenerObj:listenersList){
			listenerObj.notifyAgentStatusChange(this);
		}
	}

	public AgentStatus getStatus() {

		return status;
	}

	public void setStatus(AgentStatus status) {
		this.status=status;
		
	}

	public Role getRole() {
		// TODO Auto-generated method stub
		return role;
	}



	public String getId() {
		// TODO Auto-generated method stub
		return id;
	}



}

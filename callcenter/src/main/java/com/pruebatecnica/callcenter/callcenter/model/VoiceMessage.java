package com.pruebatecnica.callcenter.callcenter.model;

import java.io.Serializable;

public class VoiceMessage implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5327529057932621548L;
	private Customer customer;
	private long time;
	private byte[] content;
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
	
}

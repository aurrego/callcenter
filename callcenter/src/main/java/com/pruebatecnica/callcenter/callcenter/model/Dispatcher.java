package com.pruebatecnica.callcenter.callcenter.model;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.pruebatecnica.callcenter.callcenter.model.Role.AgentServiceLeveL;
import com.pruebatecnica.callcenter.callcenter.rule.CallAgentsLevel1;

public class Dispatcher implements IDispatcher, ICallListener {
	private ConcurrentLinkedQueue<Call> callQueue = new ConcurrentLinkedQueue<Call>();
	// Operadores
	private ConcurrentLinkedQueue<ICallAgent> agentQueueSerLevel1 = new ConcurrentLinkedQueue<ICallAgent>();
	// supervisores
	private ConcurrentLinkedQueue<ICallAgent> agentQueueSerLevel2 = new ConcurrentLinkedQueue<ICallAgent>();
	// Directores
	private ConcurrentLinkedQueue<ICallAgent> agentQueueSerLevel3 = new ConcurrentLinkedQueue<ICallAgent>();
	private ConcurrentHashMap<String, Integer> attentionStats = new ConcurrentHashMap<String, Integer>();

	public boolean dispatchCall(Call call) {
		CallAgentsLevel1 callRule = new CallAgentsLevel1();
		return callRule.process(this, call);
	}

	private ConcurrentLinkedQueue<ICallAgent> getAgentQueue(ICallAgent agent) {
		ConcurrentLinkedQueue<ICallAgent> queueToReturn = null;
		switch (agent.getRole().getServiceLevel()) {
		case OPERADOR:// Operador
			queueToReturn = agentQueueSerLevel1;
			break;
		case SUPERVISOR:
			queueToReturn = agentQueueSerLevel2;
			break;
		case DIRECTOR:
			queueToReturn = agentQueueSerLevel3;
			break;
		default:
			queueToReturn = agentQueueSerLevel1;
			break;
		}
		return queueToReturn;

	}

	public void notifyAgentStatusChange(ICallAgent agent) {
		if (ICallAgent.AgentStatus.AVAILABLE.equals(agent.getStatus())) {
			ConcurrentLinkedQueue<ICallAgent> queue = getAgentQueue(agent);
			queue.offer(agent);
			if (callQueue.peek() != null) {
				Call call = callQueue.poll();
				dispatchCall(call);
			}
		}

	}

	public void notifyCallStatusChange(Call call) {
		// TODO Hacer algo con las llamadas al cambiar de estado

	}

	public ConcurrentLinkedQueue<Call> getCallQueue() {
		return callQueue;
	}

	public void setCallQueue(ConcurrentLinkedQueue<Call> callQueue) {
		this.callQueue = callQueue;
	}

	public ConcurrentLinkedQueue<ICallAgent> getAgentQueueSerLevel1() {
		return agentQueueSerLevel1;
	}

	public void setAgentQueueSerLevel1(ConcurrentLinkedQueue<ICallAgent> agentQueueSerLevel1) {
		this.agentQueueSerLevel1 = agentQueueSerLevel1;
	}

	public ConcurrentLinkedQueue<ICallAgent> getAgentQueueSerLevel2() {
		return agentQueueSerLevel2;
	}

	public void setAgentQueueSerLevel2(ConcurrentLinkedQueue<ICallAgent> agentQueueSerLevel2) {
		this.agentQueueSerLevel2 = agentQueueSerLevel2;
	}

	public ConcurrentLinkedQueue<ICallAgent> getAgentQueueSerLevel3() {
		return agentQueueSerLevel3;
	}

	public void setAgentQueueSerLevel3(ConcurrentLinkedQueue<ICallAgent> agentQueueSerLevel3) {
		this.agentQueueSerLevel3 = agentQueueSerLevel3;
	}

	public boolean addAgent(ICallAgent agent) {
		ConcurrentLinkedQueue<ICallAgent> queue = getAgentQueue(agent);
		agent.addStatusListener(this);
		return queue.add(agent);

	}

	public ConcurrentHashMap<String, Integer> getAttentionStats() {
		return attentionStats;
	}

	/**
	 * Incrementa el contador de atención por rol
	 */
	public void addStats(AgentServiceLeveL agentLevel) {
		Integer counter = this.attentionStats.get(agentLevel.name());
		if (counter == null) {
			counter = 0;
		}
		counter = counter.intValue() + 1;
		this.attentionStats.put(agentLevel.name(), counter);
	}
}

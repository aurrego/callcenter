package com.pruebatecnica.callcenter.callcenter;

import com.pruebatecnica.callcenter.callcenter.model.Call;
import com.pruebatecnica.callcenter.callcenter.model.ICallService;

/**
 * Lanza las llamadas en un hilo independiente
 */
public class CallLauncher implements Runnable {
	private ICallService service;
	private Call call;

	public CallLauncher(ICallService service, Call call) {
		this.service = service;
		this.call = call;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		service.receiveCall(call);
	}

}

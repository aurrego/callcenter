package com.pruebatecnica.callcenter.callcenter.rule;

import java.util.Random;

import com.pruebatecnica.callcenter.callcenter.model.Call;
import com.pruebatecnica.callcenter.callcenter.model.Dispatcher;
import com.pruebatecnica.callcenter.callcenter.model.VoiceMail;
import com.pruebatecnica.callcenter.callcenter.model.Role.AgentServiceLeveL;
/**
 * Resuelve la cuarta instancia de atención de llamadas (Encolar la llamada hasta que un agente este libre)
 * */
public class CallQueueRule implements IProcessCall {
	public static final int MAX_QUEUESIZE=10;

	public boolean process(Dispatcher dispatcher, Call incomingCall) {
		
		if(dispatcher.getCallQueue().size()<MAX_QUEUESIZE){
			System.out.println("La llamada con id "+incomingCall.getId()+" esta en cola de espera");
			
			if(dispatcher.getCallQueue().offer(incomingCall)){
				incomingCall.setStatus(Call.CallStatus.WAITING);
				dispatcher.addStats(AgentServiceLeveL.COLA);
			}
			
			return true;
		}
		//La cola esta llena y no hay Agentes Libres
		Random rand= new Random();
		VoiceMail voiceMail= new VoiceMail((rand.nextInt(20000)+""));
		voiceMail.connectCall(incomingCall);
		
		return false;
	}

}

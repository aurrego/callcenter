package com.pruebatecnica.callcenter.callcenter.model;

import java.io.Serializable;

public class Role implements Serializable{
	public enum AgentServiceLeveL{OPERADOR,SUPERVISOR,DIRECTOR,COLA}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1419616777989592952L;
	private String name;
	private AgentServiceLeveL serviceLevel;
	public Role(String name,AgentServiceLeveL serviceLevel) {
this.name=name;
this.serviceLevel=serviceLevel;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public AgentServiceLeveL getServiceLevel() {
		return serviceLevel;
	}
	public void setServiceLevel(AgentServiceLeveL serviceLevel) {
		this.serviceLevel = serviceLevel;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
}

package com.pruebatecnica.callcenter.callcenter.rule;

import com.pruebatecnica.callcenter.callcenter.model.Call;
import com.pruebatecnica.callcenter.callcenter.model.Dispatcher;
import com.pruebatecnica.callcenter.callcenter.model.ICallAgent;
import com.pruebatecnica.callcenter.callcenter.model.ICallAgent.AgentStatus;
import com.pruebatecnica.callcenter.callcenter.model.Role.AgentServiceLeveL;
/**
 * Resuelve la primera instancia de atención de llamadas (Operador)
 * */
public class CallAgentsLevel1 implements IProcessCall {

	public boolean process(Dispatcher dispatcher,Call incomingCall) {
		ICallAgent agent=dispatcher.getAgentQueueSerLevel1().poll();
		if(agent!=null){
			//TODO Poner un logger
			System.out.println("LLamada "+incomingCall.getId()+" asignada a un "+agent.getRole().getServiceLevel()+" con id "+agent.getId()+". Quedan "+dispatcher.getAgentQueueSerLevel1().size()+" "+agent.getRole().getServiceLevel()+" disponibles");
			agent.setStatus(AgentStatus.BUSY);
			 agent.connectCall(incomingCall);
			 dispatcher.addStats(AgentServiceLeveL.OPERADOR);
			 return true;
		}
		CallAgentsLevel2 ruleProcess= new CallAgentsLevel2();
		return ruleProcess.process(dispatcher, incomingCall);
	}

}

package com.pruebatecnica.callcenter.callcenter;

import java.util.Calendar;

import com.pruebatecnica.callcenter.callcenter.model.Agent;
import com.pruebatecnica.callcenter.callcenter.model.Call;
import com.pruebatecnica.callcenter.callcenter.model.CallService;
import com.pruebatecnica.callcenter.callcenter.model.ICallService;
import com.pruebatecnica.callcenter.callcenter.model.Role;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {

		CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR", Role.AgentServiceLeveL.OPERADOR), "1"));
		CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR", Role.AgentServiceLeveL.OPERADOR), "2"));
		CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR", Role.AgentServiceLeveL.OPERADOR), "3"));
		CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR", Role.AgentServiceLeveL.OPERADOR), "4"));
		CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR", Role.AgentServiceLeveL.OPERADOR), "5"));
		CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR", Role.AgentServiceLeveL.OPERADOR), "6"));
		CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR", Role.AgentServiceLeveL.OPERADOR), "7"));
		CallService.getInstance().addCallAgent(new Agent(new Role("OPERADOR", Role.AgentServiceLeveL.OPERADOR), "8"));
		CallService.getInstance()
				.addCallAgent(new Agent(new Role("SUPERVISOR", Role.AgentServiceLeveL.SUPERVISOR), "9"));
		CallService.getInstance()
				.addCallAgent(new Agent(new Role("SUPERVISOR", Role.AgentServiceLeveL.DIRECTOR), "10"));
		for (int i = 0; i < 25; i++) {
			Call call = new Call();
			call.setId(i + "");
			call.setStartTime(Calendar.getInstance().getTime().getTime());
			ICallService service = CallService.getInstance();
			Thread launcherThread = new Thread(new CallLauncher(service, call));
			launcherThread.start();
		}
	}
}

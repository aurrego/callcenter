package com.pruebatecnica.callcenter.callcenter.model;
/**
 * Firma de un Agente de atención de llamadas que puede ser un Operador, un supervisor, director o el VoiceMail cuando las colas estan llenas
 * */
public interface ICallAgent {
	public enum AgentStatus{ AVAILABLE,BUSY,DISCONNECTED}
	
	Call connectCall(Call incomingCall);
	boolean addStatusListener(ICallListener listener);
	boolean removeStatusListener(ICallListener listener);
	void notifyStatusListeners();
	AgentStatus getStatus();
	void setStatus(AgentStatus status);
	Role getRole();
	String getId();
}

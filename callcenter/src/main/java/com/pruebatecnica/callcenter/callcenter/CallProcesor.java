package com.pruebatecnica.callcenter.callcenter;

import java.util.Calendar;
import java.util.Random;

import com.pruebatecnica.callcenter.callcenter.model.Call;
import com.pruebatecnica.callcenter.callcenter.model.ICallAgent;
import com.pruebatecnica.callcenter.callcenter.model.ICallAgent.AgentStatus;

/**
 * Simula la atención de la llamada con un tiempo aleatorio entre 5 y diez seg.
 */
public class CallProcesor implements Runnable {
	private ICallAgent agent;
	private Call call;

	public CallProcesor(ICallAgent agent, Call call) {
		this.call = call;
		this.agent = agent;
	}

	@Override
	@SuppressWarnings("static-access")
	public void run() {
		Random rand = new Random();
		int callDuration = rand.nextInt(11);
		if (callDuration < 5) {
			callDuration = 5;
		}
		try {
			Thread.currentThread();
			Thread.sleep(callDuration * 1000);
		} catch (InterruptedException e) {
			System.out.println("Ocurrio un error la atender la llamada con id " + call.getId()
					+ ", el agente quedó libre " + agent.getId());
		}
		call.setEndTime(Calendar.getInstance().getTime().getTime());
		System.out.println("******Terminó el procesamiento de la llamada con id " + call.getId() + " tardo "
				+ ((call.getEndTime() - call.getStartTime()) / 1000) + "seg, el Agente con id " + agent.getId()
				+ " y rol " + agent.getRole().getServiceLevel() + " quedó Libre.");
		call.disconnect();
		agent.setStatus(AgentStatus.AVAILABLE);
		agent.notifyStatusListeners();
	}

}

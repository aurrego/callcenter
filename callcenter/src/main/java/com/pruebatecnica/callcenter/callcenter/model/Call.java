package com.pruebatecnica.callcenter.callcenter.model;

import java.io.Serializable;

public class Call implements Serializable {
	/**
	 * 
	 */
	public enum CallStatus {
		WAITING, CONNECTED, DISCONNECTED,INCOMING
	}

	private static final long serialVersionUID = 824106877163916248L;
	private String phoneNumber;
	private long startTime;
	private long endTime;
	private String id;
	private CallStatus status=CallStatus.INCOMING;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public CallStatus getStatus() {
		return status;
	}

	public void setStatus(CallStatus status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean disconnect() {
		System.out.println("La llamada con Id "+this.id+" ha terminado");
		this.setStatus(CallStatus.DISCONNECTED);
		return true;
	}

}

package com.pruebatecnica.callcenter.callcenter.model;

/**
 * Observer de los eventos de Atención de llamadas (Llamadas y agentes)
 * */
public interface ICallListener {
	void notifyAgentStatusChange( ICallAgent agent);
	void notifyCallStatusChange(Call call);
}

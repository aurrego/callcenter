package com.pruebatecnica.callcenter.callcenter.rule;

import com.pruebatecnica.callcenter.callcenter.model.Call;
import com.pruebatecnica.callcenter.callcenter.model.Dispatcher;

public interface IProcessCall {

	boolean process(Dispatcher dispatcher,Call incomingCall);
}
